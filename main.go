package main

import (
	_ "embed"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
)

var (
	//go:embed resources/index.html
	html       []byte
	adjectives = []string{"Funny", "Silly", "Crazy", "Goofy", "Wacky", "Loony", "Zany"}
	nouns      = []string{"Chicken", "Goose", "Giraffe", "Koala", "Lemur", "Quokka", "Axolotl"}
)

func generateFunnyName() string {
	adjective := adjectives[rand.Intn(len(adjectives))]
	noun := nouns[rand.Intn(len(nouns))]
	return adjective + " " + noun
}

func main() {
	var httpPort = os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	message := generateFunnyName()

	htmlPage := strings.Replace(
		string(html),
		"{{message}}",
		message,
		-1,
	)
	//fmt.Println(len(htmlPage))

	mux := http.NewServeMux()

	mux.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {
		response.Header().Add("Content-Type", "text/html;charset=utf-8")
		response.Write([]byte(htmlPage))
	})

	var errListening error
	log.Println("🌍 http server is listening on: " + httpPort)
	errListening = http.ListenAndServe(":"+httpPort, mux)

	log.Fatal(errListening)
}
