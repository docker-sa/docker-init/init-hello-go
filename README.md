# Init "Hello Go"

```bash
docker init
```
Then follow the instructions of the generated `README.Docker.md` file.

To reset the project, run `./raz.sh`.